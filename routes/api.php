<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::middleware('auth:sanctum')->group( function (){

    Route::get('/user', function (Request $request) {
        return $request->user();
    });


    Route::post('/logout', [\App\Http\Controllers\Api\Auth\AuthController::class, 'logout']);

    /*** Users routes resource ***/
    Route::apiResource('/users', \App\Http\Controllers\Api\Users\UsersController::class);
    Route::apiResource('/customers', \App\Http\Controllers\Api\Customers\CustomersController::class);

    Route::get('dashboard/customers', [\App\Http\Controllers\Api\Dashboard\DashboardController::class, 'getCustomersCount']);
    Route::get('dashboard/customers-feedbacks', [\App\Http\Controllers\Api\Dashboard\DashboardController::class, 'getCustomersFeedback']);
});

Route::post('/login', [\App\Http\Controllers\Api\Auth\AuthController::class, 'login']);
Route::apiResource('/feedbacks', \App\Http\Controllers\Api\Feedbacks\FeedbacksController::class);

