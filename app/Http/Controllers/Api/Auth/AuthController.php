<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\ViewModels\Auth\AuthViewModelInterface;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function __construct(protected AuthViewModelInterface $authViewModel)
    {
    }

    public function login(LoginRequest $request){
        return $this->authViewModel->login($request);
    }

    public function logout(){
        return $this->authViewModel->logout();
    }
}
