<?php

namespace App\Http\Controllers\Api\Feedbacks;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFeedbackRequest;
use App\Http\Requests\UpdateFeedbackRequest;
use App\Http\Resources\FeedbackResource;
use App\Mail\DetractorMail;
use App\Models\Customer;
use App\Models\Feedback;
use Illuminate\Support\Facades\Mail;

class FeedbacksController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Feedback::leftjoin('customers', 'customers.id', 'feedbacks.customer_id')
            ->select('feedbacks.id','customers.name','customers.phone','customers.email','customers.customer_category_id','feedbacks.created_at',
                'feedbacks.product_experience','feedbacks.customer_care','feedbacks.product_satisfaction','feedbacks.mean')
            ->orderBy('feedbacks.id', 'desc')
            ->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFeedbackRequest $request)
    {
        $data = $request->validated();
        $customer = Customer::query()->where('email', $data['email'])->first();

        if(!$customer){
            $customer = Customer::create($data);
        }

        $data['customer_id'] = $customer->id;
        $data['mean'] = round(($data['product_experience'] + $data['customer_care'] + $data['product_satisfaction']) / 3, 2);

        $feedback = Feedback::create($data);

        if($data['mean'] < 3){
            $this->getDetractorNotification($customer, $feedback);
        }

        return new FeedbackResource($feedback);
    }

    private function getDetractorNotification($customer, $feedback){
        Mail::to('timothymuia17@gmail.com')->send(new DetractorMail($customer, $feedback));
    }

    /**
     * Display the specified resource.
     */
    public function show(Feedback $feedback)
    {
        return  Feedback::leftjoin('customers', 'customers.id', 'feedbacks.customer_id')
            ->where('feedbacks.id', $feedback->id)
            ->select('feedbacks.id','customers.name','customers.phone','customers.email','customers.customer_category_id','feedbacks.created_at',
                'feedbacks.product_experience','feedbacks.customer_care','feedbacks.product_satisfaction','feedbacks.mean')
            ->orderBy('feedbacks.id', 'desc')
            ->first();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFeedbackRequest $request, Feedback $feedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Feedback $feedback)
    {
        $feedback->delete();

        return response('', 204);
    }
}
