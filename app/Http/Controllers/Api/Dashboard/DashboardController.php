<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Feedback;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    const BUYERS = 1;
    const PAINTERS = 2;
    const ACTIVE = 1;

    public function getCustomersCount(){

        $data = [
          'all' => Customer::query()->where('active', self::ACTIVE)->count('id'),
          'buyers' => Customer::query()->where([['active', self::ACTIVE], ['customer_category_id', self::BUYERS]])->count('id'),
          'painters' => Customer::query()->where([['active', self::ACTIVE], ['customer_category_id', self::PAINTERS]])->count('id')
        ];

        return response($data, 200);
    }

    public function getCustomersFeedback(){

        return [
            'line' => [
                'labels' => $this->getWeekDays(),
                'series' =>[
                    'buyers' => $this->getCustomerCategoryWeeklyCount(self::BUYERS),
                    'painters' => $this->getCustomerCategoryWeeklyCount(self::PAINTERS)
                ]
            ],
            'donut' => [
                'labels' => $this->getMeanDescription(),
                'series' => $this->getFeebbackMeanCount()
            ]
        ];

    }

    private function getWeekDays(){
        $days = [];

        for($i = 0; $i < 7; $i++ ){
            $days[] = Carbon::now()->subDays($i)->format('d M');
        }

        return array_reverse($days);
    }

    private function getMeanDescription(){

        return ['Detractors','Passives','Promoters'];
    }

    private function getFeebbackMeanCount(){

        return [
            Feedback::query()->where('mean','<', 3)->count('id'),
            Feedback::query()->where('mean','=', 3)->count('id'),
            Feedback::query()->where('mean','>', 3)->count('id')
        ];
    }

    private function getCustomerCategoryWeeklyCount($customer_category_id){

        $feedbacks = [];

        for($i = 0; $i < 7; $i++ ){
            $date = Carbon::now()->subDays($i)->format('Y-m-d');

            $feedback = Feedback::query()->whereDate('created_at', $date)->where('customer_category_id', $customer_category_id)->count('id');
            $feedbacks[] = $feedback;
        }

        return array_reverse($feedbacks);
    }
}
