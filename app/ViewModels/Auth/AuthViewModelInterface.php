<?php

namespace App\ViewModels\Auth;

use App\Http\Requests\Auth\LoginRequest;

interface AuthViewModelInterface
{

    public function login(LoginRequest$request);

    public function logout();
}
