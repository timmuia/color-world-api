<?php

namespace App\ViewModels\Auth;

use App\Http\Requests\Auth\LoginRequest;
use App\Interfaces\Api\Auth\AuthInterface;

class AuthViewModel implements AuthViewModelInterface
{

    public function __construct(protected AuthInterface $auth)
    {
    }


    /**
     * @param LoginRequest $request
     * @return void
     */
    public function login(LoginRequest $request)
    {

        $payload = $request->validated();
        return $this->auth->login($payload);
    }

    public function logout()
    {
        return $this->auth->logout();
    }
}
