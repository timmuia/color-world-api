<?php

namespace App\Repositories\Api\Auth;

use App\Interfaces\Api\Auth\AuthInterface;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthRepository implements AuthInterface
{

    public function login($payload)
    {
        // TODO: Implement login() method.


        if(!Auth::attempt($payload)){
            return response([
                'message' => 'Provided credentials are incorrect!'
            ], 422);
        }

        /** @var User $user */
        $user = Auth::user();

        $token = $user->createToken('main')->plainTextToken;

        return response(compact('user','token'));
    }

    public function logout()
    {
        /** @var User $user */

        $user = request()->user();

        $user->currentAccessToken()->delete();

        return response('', 204);
    }
}
