<?php

namespace App\Providers;

use App\ViewModels\Auth\AuthViewModel;
use App\ViewModels\Auth\AuthViewModelInterface;
use Illuminate\Support\ServiceProvider;

class ViewModelProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(AuthViewModelInterface::class, AuthViewModel::class);
    }
}
