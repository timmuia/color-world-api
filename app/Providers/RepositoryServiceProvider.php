<?php

namespace App\Providers;

use App\Interfaces\Api\Auth\AuthInterface;
use App\Repositories\Api\Auth\AuthRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(AuthInterface::class, AuthRepository::class);
    }
}
