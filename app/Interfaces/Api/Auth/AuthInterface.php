<?php

namespace App\Interfaces\Api\Auth;

interface AuthInterface
{

    public function login($payload);

    public function logout();
}
