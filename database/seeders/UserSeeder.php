<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
           'name' => 'Timothy Muia',
            'email' => 'timothymuia17@gmail.com',
            'password' => Hash::make('timothymuia17@gmail.com')
        ]);
    }
}
