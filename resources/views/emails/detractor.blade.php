<!Doctype html>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <head></head>
</html>
<body>
<div style="display: flex; justify-content: center; align-content: center; padding: 100px; background-color: #e5e7eb;">
    <div style="background-color: white; padding: 50px 30px 50px 30px; border-radius: 5px; ">
        Hello
        <br><br>
        Please find here the details of a customers complaint.
        <div style="display: flex; flex-direction: column">
            <div>Name: {{ $customer->name }}</div>
            <div>Phone: {{ $customer->phone }}</div>
            <div>Email: {{ $customer->email }}</div>
        </div>
        <br><br>
        Regards,
        <br>
        Color World
    </div>
</div>
</body>
